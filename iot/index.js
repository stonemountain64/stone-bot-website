'use strict';

const awsIot = require('aws-iot-device-sdk');

let client, iotTopic;
const IoT = {

    connect: (topic, iotEndpoint, region, accessKey, secretKey, sessionToken) => {

        iotTopic = topic;

        client = awsIot.device({
            region: region,
            protocol: 'wss',
            accessKeyId: accessKey,
            secretKey: secretKey,
            sessionToken: sessionToken,
            port: 443,
            host: iotEndpoint
        });

        client.on('connect', onConnect);
        client.on('message', onMessage);
        client.on('error', onError);
        client.on('reconnect', onReconnect);
        client.on('offline', onOffline);
        client.on('close', onClose);
    },

    send: (message) => {
        client.publish(iotTopic, message);
    }
};

const onConnect = () => {
    client.subscribe(iotTopic);
    //addLog('Connected');
};

const onMessage = (topic, message) => {
    addLog(message);
};

const onError = () => {
	//addLog('Error');
};
const onReconnect = () => {};
const onOffline = () => {};

const onClose = () => {
    addLog('Connection failed');
};

$(document).ready(() => {

	setTimeout(set_up_keys(), 10);
    $( "#draggable" ).draggable();

});

function set_up_connection(iotKeys) {
	const iotTopic = '/serverless/pubsub';        

	IoT.connect(iotTopic,
				iotKeys.iotEndpoint,
				iotKeys.region,
				iotKeys.accessKey,
				iotKeys.secretKey,
				iotKeys.sessionToken);
				
				
	addLog(':)');
}
function set_up_keys() {
   let iotKeys;

    $.ajax({
        url: window.lambdaEndpoint,
        success: (res) => {



            iotKeys = res; // save the keys

			setTimeout(set_up_connection(iotKeys), 1000);


		}

    });
}

const addLog = (msg) => {
    $('#log').hide();
    $("#log").text(msg);
    //$("#log").show('slow');
    
      var selectedEffect = $( "#effectTypes" ).val();
      // Most effect types need no options passed by default
      var options = {};
      // some effects have required parameters
      if ( selectedEffect === "scale" ) {
        options = { percent: 50 };

      } else if ( selectedEffect === "size" ) {
        options = { to: { width: 280, height: 185 } };
      }
      // Run the effect

      $( "#log" ).show( selectedEffect, options, 500 );

    
    //$('#log').delay(5000).hide('slow');
    //setTimeout(runEffect(), 5000);
    setTimeout(function(){
       runEffect();
    },5000);
    //$("#log").show('slide').delay(5000).hide('slide');
}

function runEffect() {
  // get effect type from
  var selectedEffect = $( "#effectTypes" ).val();

  // Most effect types need no options passed by default
  var options = {};
  // some effects have required parameters
  if ( selectedEffect === "scale" ) {
    options = { percent: 50 };
  } else if ( selectedEffect === "size" ) {
    options = { to: { width: 200, height: 60 } };
  }

  $( "#log" ).hide( selectedEffect, options, 1000);
}


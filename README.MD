# This is the Stone-Bot website repository

Current Build Status: [![pipeline status](https://gitlab.com/stonemountain64/stone-bot-website/badges/master/pipeline.svg)](https://gitlab.com/stonemountain64/stone-bot-website/commits/master)

-----

Current Site: http://stone-bot-notifications.s3-website-us-east-1.amazonaws.com/index.html
